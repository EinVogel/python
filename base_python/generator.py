gen = (i for i in range(2, 8))
for i in gen:
    print(i)


def func(start, finish):
    while start < finish:
        yield start * 0.33
        start += 1


gen = func(1, 4)

for i in gen:
    print(i)
