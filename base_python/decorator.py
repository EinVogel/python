import time


def time_measure(func):
    def wrapped(*args, **kwargs):
        start_time = time.time()
        answer = func(*args, **kwargs)
        end_time = time.time()
        print(end_time - start_time)
        return answer

    return wrapped


@time_measure
def hello():
    return 'hello'


def bread(func):
    def wrapper():
        print("</------\>")
        func()
        print("<\______/>")

    return wrapper


def ingredients(func):
    def wrapper():
        print("#помидоры#")
        func()
        print("~салат~")

    return wrapper


def sandwich(food="--ветчина--"):
    print(food)


sandwich = bread(ingredients(sandwich))
sandwich()
